import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
	
	public static void main(String[] args) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("carros");
		
		EntityManager manager = factory.createEntityManager();
		
		Carro carro = new Carro("AED-1234", "VW", "Fusca", 1978);
		
		manager.getTransaction().begin();
		manager.persist(carro);
		manager.getTransaction().commit();
		
		manager.close();
		factory.close();
		
	}

}
